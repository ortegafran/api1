package com.ar.donko.primerApi;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class Mensajes {

    @GetMapping("/")
    public String holaMundo(){
        return "Hola Mundo!";

    }

    @GetMapping("/chaumundo")
    public String chauMundo(){
        return "chau Mundo!";
    }
}
