package com.ar.donko.primerApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrimerApiApplication {

	public static void main(String[] args) {
		try{
			SpringApplication.run(PrimerApiApplication.class, args);
		}catch (Throwable t){
			t.printStackTrace();
		}
	}

}
